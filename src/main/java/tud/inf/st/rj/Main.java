package tud.inf.st.rj;

import org.ros.node.*;
import org.ros.node.NodeMainExecutor;

import java.net.URI;
import java.net.URISyntaxException;


public class Main {

    private static Talker talker;
    private static Listener listener;

    public static void main(String[] args) throws URISyntaxException {

        // setup default configs for node execution
        NodeMainExecutor nodeMainExecutor = DefaultNodeMainExecutor.newDefault();
        NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic("localhost");
        nodeConfiguration.setMasterUri(new URI("http://localhost:11311"));

        // create a talker an a listener
        talker = new Talker();
        listener = new Listener();

        // start a listener thread
        new Thread(() -> {
            nodeMainExecutor.execute(listener, nodeConfiguration);
        }) {{
            start();
        }};

        // start a talker thread
        new Thread(() -> {
            nodeMainExecutor.execute(talker, nodeConfiguration);
        }) {{
            start();
        }};
    }
}
