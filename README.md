# rosjava_gradle

## How to start it

* Run roscore in a console
* Import into IntelliJ as Gradle Project
* Run it by right-clicking on the main class and clicking the "Play" button.

## Code Sources

* Where can I find important imports? -> https://github.com/rosjava/android_core
* This code is based on an analysis of rosjavas android code
    * http://docs.ros.org/en/hydro/api/android_apps/html/Listener_8java_source.html
    * http://wiki.ros.org/rosjava/Tutorials/Create%20a%20ROS%20Android%20Node
    * https://github.com/rosjava/android_core/tree/kinetic/android_tutorial_pubsub
